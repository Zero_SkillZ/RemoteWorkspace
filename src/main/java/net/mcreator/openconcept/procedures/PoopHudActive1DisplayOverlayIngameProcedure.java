package net.mcreator.openconcept.procedures;

import net.minecraft.entity.Entity;

import net.mcreator.openconcept.OpenConceptModVariables;
import net.mcreator.openconcept.OpenConceptMod;

import java.util.Map;

public class PoopHudActive1DisplayOverlayIngameProcedure {
	public static boolean executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				OpenConceptMod.LOGGER.warn("Failed to load dependency entity for procedure PoopHudActive1DisplayOverlayIngame!");
			return false;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if ((((entity.getCapability(OpenConceptModVariables.PLAYER_VARIABLES_CAPABILITY, null)
				.orElse(new OpenConceptModVariables.PlayerVariables())).PoopHudTimer) >= 12000)) {
			return (true);
		}
		return (false);
	}
}
