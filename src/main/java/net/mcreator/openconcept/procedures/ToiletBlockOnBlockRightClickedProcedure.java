package net.mcreator.openconcept.procedures;

public class ToiletBlockOnBlockRightClickedProcedure {

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				OpenConceptMod.LOGGER.warn("Failed to load dependency entity for procedure ToiletBlockOnBlockRightClicked!");
			return;
		}

		Entity entity = (Entity) dependencies.get("entity");

		if (((((entity.getCapability(OpenConceptModVariables.PLAYER_VARIABLES_CAPABILITY, null)
				.orElse(new OpenConceptModVariables.PlayerVariables())).PoopHudTimer) >= 12000)
				&& (((entity.getCapability(OpenConceptModVariables.PLAYER_VARIABLES_CAPABILITY, null)
						.orElse(new OpenConceptModVariables.PlayerVariables())).PoopHudTimer) < 24000))) {
			{
				double _setval = (double) (((entity.getCapability(OpenConceptModVariables.PLAYER_VARIABLES_CAPABILITY, null)
						.orElse(new OpenConceptModVariables.PlayerVariables())).PoopHudTimer) - 12000);
				entity.getCapability(OpenConceptModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
					capability.PoopHudTimer = _setval;
					capability.syncPlayerVariables(entity);
				});
			}
			if (entity instanceof PlayerEntity) {
				ItemStack _setstack = new ItemStack(FertilizerItem.block);
				_setstack.setCount((int) 1);
				ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) entity), _setstack);
			}
		} else if (((((entity.getCapability(OpenConceptModVariables.PLAYER_VARIABLES_CAPABILITY, null)
				.orElse(new OpenConceptModVariables.PlayerVariables())).PoopHudTimer) >= 24000)
				&& (((entity.getCapability(OpenConceptModVariables.PLAYER_VARIABLES_CAPABILITY, null)
						.orElse(new OpenConceptModVariables.PlayerVariables())).PoopHudTimer) < 36000))) {
			{
				double _setval = (double) (((entity.getCapability(OpenConceptModVariables.PLAYER_VARIABLES_CAPABILITY, null)
						.orElse(new OpenConceptModVariables.PlayerVariables())).PoopHudTimer) - 24000);
				entity.getCapability(OpenConceptModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
					capability.PoopHudTimer = _setval;
					capability.syncPlayerVariables(entity);
				});
			}
			if (entity instanceof PlayerEntity) {
				ItemStack _setstack = new ItemStack(FertilizerItem.block);
				_setstack.setCount((int) 2);
				ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) entity), _setstack);
			}
		} else if (((((entity.getCapability(OpenConceptModVariables.PLAYER_VARIABLES_CAPABILITY, null)
				.orElse(new OpenConceptModVariables.PlayerVariables())).PoopHudTimer) >= 36000)
				&& (((entity.getCapability(OpenConceptModVariables.PLAYER_VARIABLES_CAPABILITY, null)
						.orElse(new OpenConceptModVariables.PlayerVariables())).PoopHudTimer) < 48000))) {
			{
				double _setval = (double) (((entity.getCapability(OpenConceptModVariables.PLAYER_VARIABLES_CAPABILITY, null)
						.orElse(new OpenConceptModVariables.PlayerVariables())).PoopHudTimer) - 36000);
				entity.getCapability(OpenConceptModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
					capability.PoopHudTimer = _setval;
					capability.syncPlayerVariables(entity);
				});
			}
			if (entity instanceof PlayerEntity) {
				ItemStack _setstack = new ItemStack(FertilizerItem.block);
				_setstack.setCount((int) 3);
				ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) entity), _setstack);
			}
		} else if ((((entity.getCapability(OpenConceptModVariables.PLAYER_VARIABLES_CAPABILITY, null)
				.orElse(new OpenConceptModVariables.PlayerVariables())).PoopHudTimer) >= 48000)) {
			{
				double _setval = (double) 0;
				entity.getCapability(OpenConceptModVariables.PLAYER_VARIABLES_CAPABILITY, null).ifPresent(capability -> {
					capability.PoopHudTimer = _setval;
					capability.syncPlayerVariables(entity);
				});
			}
			if (entity instanceof PlayerEntity) {
				ItemStack _setstack = new ItemStack(FertilizerItem.block);
				_setstack.setCount((int) 3);
				ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) entity), _setstack);
			}
		}
	}

}
